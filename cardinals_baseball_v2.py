import pandas as pd
import numpy as np
import re
import sys, os

if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
 
elif not os.path.exists(sys.argv[1]):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

else:
    filename = sys.argv[1]
    stats_df = pd.DataFrame(columns=['NAME','AT_BATS','HITS'])
    player_list = []
    gamePat = "===.+?==="
    pattern = r"(?P<name>\b\w* \w*\b)\D*(?P<abs>\b\d*\b)\D*(?P<hits>\b\d*\b)"
    with open(filename,"r") as f:
        #Skip the first four lines of the txt file
        for _ in xrange(4):
            next(f)
        for line in f:
            #Strip the lines to get rid of any white space
            lineStripped = line.strip()
            #Detect and skip lines with game description or empty lines
            if(re.match(gamePat, lineStripped) != None or len(lineStripped) == 0):
                pass
            else:
                match = re.match(pattern, line)
                name = match.group("name")
                atbats = match.group("abs")
                hits = match.group("hits")
                if(stats_df['NAME'].str.contains(name).sum() > 0):
                    #Add hits and at bats to the player's row 
                    prevHits = stats_df['HITS'].loc[stats_df['NAME'] == name]
                    prevAbs = stats_df['AT_BATS'].loc[stats_df['NAME'] == name]
                    stats_df['HITS'].loc[stats_df['NAME'] == name] = int(prevHits) + int(hits)
                    stats_df['AT_BATS'].loc[stats_df['NAME'] == name] = int(prevAbs) + int(atbats)
                else:
                    #Add the name, at bats, and hits to a new dataframe, append that dataframe to the master
                    player_df = pd.DataFrame({'NAME': name, 'AT_BATS': atbats, 'HITS': hits},index=[0])
                    stats_df = stats_df.append(player_df)

    #Calculate the average, round and sort
    stats_df['AVERAGE'] = stats_df['HITS'] / stats_df['AT_BATS']
    stats_df['AVERAGE'] = np.around(stats_df['AVERAGE'].astype(np.double),3)
    stats_df.sort_values(['AVERAGE','NAME'],ascending=False,inplace=True)
    #iterrate over dataframe to print according to the wiki
    for index, row in stats_df.iterrows():
        print str(row['NAME']) + ': ' + "%.3f" % row['AVERAGE']
         

